extern crate chrono;
use chrono::prelude::*;
use std::collections::BTreeMap;
use std::env;
use std::path::PathBuf;
use std::fs::{File, OpenOptions};
use std::io::{BufRead, BufReader, Read, Write};
use std::process::Command;

enum WorkFile {
    Started,
    Notes,
    Log,
}

enum WorkHook {
    AfterFinish,
    BeforeStart,
    ProjectLog,
}

fn basedir() -> PathBuf {
    let mut path = PathBuf::new();

    if let Ok(dir) = env::var("WORKDIR") {
        path.push(dir);
    } else if let Ok(dir) = env::var("HOME") {
        path.push(dir);
        path.push(".work");
    }

    path
}

fn path_for(file: WorkFile) -> PathBuf {
    let mut path = basedir();

    path.push(match file {
        WorkFile::Started => "started",
        WorkFile::Notes   => "notes",
        WorkFile::Log     => "log"
    });

    path
}

fn run_hook(hook: WorkHook, args: Option<&[&str]>) {
    let mut hook_path = basedir();
    hook_path.push("hooks");
    hook_path.push(match hook {
        WorkHook::AfterFinish => "after-finish",
        WorkHook::BeforeStart => "before-start",
        WorkHook::ProjectLog  => "project-log",
    });

    let workdir = basedir();

    let args = match args {
        Some(args) => args,
        None       => &[],
    };

    Command::new(hook_path)
        .current_dir(workdir)
        .args(args)
        .spawn()
        .expect("Failed to run hook")
        .wait()
        .expect("Failed to wait on hook process");
}

fn time_roundings(seconds: i64) -> [i64; 3] {
    let multiplier = 30 * 60; // seconds in half-hour
    let mut half_hours = seconds / multiplier;

    // so we don't suggest anything smaller than [30, 60, 90] minutes
    if half_hours < 2 {
        half_hours = 2;
    }

    [
        (half_hours - 1) * multiplier,
        half_hours       * multiplier,
        (half_hours + 1) * multiplier,
    ]
}

fn abort() {
    show_status();
    std::fs::remove_file(path_for(WorkFile::Started)).expect("Failed to remove started file");
    println!("I just threw all that away");
}

fn current_notes() -> Option<Vec<String>> {
    let path = path_for(WorkFile::Notes);

    if path.exists() {
        let notefile = File::open(path).expect("Failed to open notefile");
        let reader = BufReader::new(notefile);

        Some(reader.lines().map(|s| s.unwrap()).collect::<Vec<_>>())
    } else {
        None
    }
}

fn finish() {
    let spent_time = match spent_time() {
        Some(seconds) => seconds,
        None => {
            println!("No work to finish");
            return;
        }
    };

    let options = time_roundings(spent_time);
    println!("You've worked for {}", format_seconds(spent_time));
    println!("How much time do you want to log?");
    println!("1) {}",           format_seconds(options[0]));
    println!("2) {} (default)", format_seconds(options[1]));
    println!("3) {}",           format_seconds(options[2]));

    let input = prompt("Your choice: ");
    let time_spent = match input.parse::<usize>() {
        Ok(i)  => format_seconds(options[i - 1]),
        Err(_) => {
            if input.is_empty() {
                format_seconds(options[1])
            } else {
                input
            }
        },
    };

    let project = prompt("What project was it? ");
    let mut notes = current_notes().unwrap_or_default();
    if !notes.is_empty() {
        println!("Notes from this session: ");
        for note in &notes {
            println!("{}", note);
        }
    }
    let note = prompt("Anything else to note? ");
    if !note.is_empty() {
        notes.push(note);
    }
    let logline = format!(
        "{},{},{},{}\n",
        Local::today().format("%Y-%m-%d"),
        time_spent,
        project,
        notes.join(";")
    );

    let mut fh = OpenOptions::new()
        .create(true)
        .append(true)
        .open(&path_for(WorkFile::Log))
        .expect("Failed to open logfile");
    fh.write_all(logline.as_bytes()).expect("Failed to write to logfile");

    std::fs::remove_file(path_for(WorkFile::Started)).expect("Failed to remove started file");

    if current_notes().is_some() {
        std::fs::remove_file(path_for(WorkFile::Notes)).expect("Failed to remove notes file");
    }

    run_hook(WorkHook::AfterFinish, None);
}

fn format_minutes(minutes: i64) -> String {
    let hours = minutes / 60;
    let minutes = minutes - hours * 60;
    format!("{}:{:02}", hours, minutes)
}

fn format_seconds(seconds: i64) -> String {
    format_minutes(seconds / 60)
}

fn minutes_in_timelog(time: &str) -> i64 {
    let mut parts = time.split(':').map(|s| s.parse::<i64>().unwrap());
    parts.next().unwrap() * 60 + parts.next().unwrap()
}

fn prompt(prompt: &str) -> String {
    print!("{}", prompt);
    std::io::stdout().flush().expect("Failed to flush stdout (???)");
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).expect("Failed to read stdin");
    input.trim().to_owned()
}

fn spent_time() -> Option<i64> {
    let started_path = path_for(WorkFile::Started);

    if started_path.exists() {
        let mut fh = File::open(&started_path).expect("Failed to open 'started' file");
        let mut contents = String::new();
        fh.read_to_string(&mut contents).expect("Failed to read 'started' file");
        let started = DateTime::parse_from_rfc3339(contents.trim()).expect("Failed to parse timestamp");

        Some(Local::now().timestamp() - started.timestamp())
    } else {
        None
    }
}

fn start_work() {
    if spent_time().is_some() {
        show_status();
        return;
    }

    run_hook(WorkHook::BeforeStart, None);

    let now = Local::now();
    println!("Work started at {}", now.format("%H:%M"));

    let mut fh = File::create(path_for(WorkFile::Started)).expect("Failed to create 'started' file");
    fh.write_all(now.to_rfc3339().as_bytes()).expect("Failed to write timestamp");
}

fn show_status() {
    match spent_time() {
        Some(seconds) => println!("You've been working for {}", format_seconds(seconds)),
        None          => println!("Work is currently not being done"),
    }
}

enum DateUnit {
    Day,
    Week,
    Month,
}

fn date_range(unit: DateUnit, last: bool) -> (Date<Local>, Date<Local>) {
    let mut start = Local::today();
    let mut end   = Local::today();

    match unit {
        DateUnit::Day => {
            if last {
                start = start - chrono::Duration::days(1);
                end   = end   - chrono::Duration::days(1);
            }
        },
        DateUnit::Month => {
            start = start.with_day(1).unwrap();

            if last {
                end = start.pred();
                start = end.with_day(1).unwrap();
            }
        },
        DateUnit::Week => {
            while start.weekday() != Weekday::Mon {
                start = start.pred();
            }

            while end.weekday() != Weekday::Sun {
                end = end.succ();
            }

            if last {
                start = start - chrono::Duration::weeks(1);
                end   = end   - chrono::Duration::weeks(1);
            }
        },
    }

    (start, end)
}

fn note() {
    let note = prompt("Enter your note: ");
    let mut fh = OpenOptions::new()
        .create(true)
        .append(true)
        .open(&path_for(WorkFile::Notes))
        .expect("Failed to open notefile");
    fh.write_all(note.as_bytes()).expect("Failed to write to notefile");
    fh.write_all("\n".as_bytes()).expect("Failed to write to notefile");
}

fn log() {
    let mut unit = DateUnit::Month;
    let mut last = false;

    for param in env::args().skip(2) {
        match param.as_str() {
            "this"  => last = false,
            "last"  => last = true,
            "day"   => unit = DateUnit::Day,
            "week"  => unit = DateUnit::Week,
            "month" => unit = DateUnit::Month,
            _       => panic!("Unrecognized parameter: '{}'", param),
        }
    }

    let (start, end) = date_range(unit, last);

    let logfile = File::open(path_for(WorkFile::Log)).expect("Failed to open logfile");
    let reader = BufReader::new(logfile);

    let mut projects = BTreeMap::new();

    for logline in reader.lines() {
        let logline = logline.expect("Failed to read line from logfile");
        let parts = logline.splitn(4, ',').collect::<Vec<_>>();
        let date_parts = parts[0].split('-').map(|s| s.parse::<u32>().unwrap()).collect::<Vec<_>>();
        let date = Local.ymd(date_parts[0] as i32, date_parts[1], date_parts[2]);
        if date >= start && date <= end {
            let project_logs = projects.entry(parts[2].to_owned()).or_insert_with(Vec::new);
            project_logs.push((parts[1].to_owned(), parts[3].to_owned()));
        }
    }

    let mut total_minutes = 0;

    for (project, logs) in projects {
        println!("Project {}:", project);
        let mut minutes = 0;

        for log in logs {
            println!("\t{:>6} {}", log.0, log.1);
            minutes += minutes_in_timelog(&log.0);
        }

        println!("\n\tTotal time spent: {}\n", format_minutes(minutes));
        run_hook(WorkHook::ProjectLog, Some(&[&project, &format!("{}", minutes)]));
        total_minutes += minutes;
    }
    println!("Total time spent (in all projects): {}", format_minutes(total_minutes));
}

fn usage() {
    println!("Usage: {} [start|status|finish]", env::args().nth(0).unwrap());
}

enum DispatchResult<'a, T> {
    Matched(&'a T),
    Ambiguous(Vec<&'a String>),
    NotFound,
}

fn dispatch<'a, T>(command: &str, candidates: &'a [(String, T)]) -> DispatchResult<'a, T> {
    let matches = candidates.iter()
        .filter(|c| c.0.starts_with(command))
        .collect::<Vec<_>>();
    if matches.len() == 1 {
        DispatchResult::Matched(&matches[0].1)
    } else if matches.is_empty() {
        DispatchResult::NotFound
    } else {
        DispatchResult::Ambiguous(matches.iter().map(|c| &c.0).collect::<Vec<_>>())
    }
}

fn main() {
    let commands: Vec<(String, Box<dyn Fn()>)> = vec![
        (String::from("abort"),  Box::new(abort)),
        (String::from("log"),    Box::new(log)),
        (String::from("note"),   Box::new(note)),
        (String::from("start"),  Box::new(start_work)),
        (String::from("status"), Box::new(show_status)),
        (String::from("finish"), Box::new(finish)),
    ];
    match env::args().nth(1) {
        Some(command) => match dispatch(&command, &commands) {
            DispatchResult::Matched(target) => target(),
            DispatchResult::Ambiguous(matches) => {
                println!("More than one command matches prefix '{}':", command);
                for m in matches {
                    println!("\t{}", m);
                }
                println!("Please be more specific");
            },
            DispatchResult::NotFound => usage(),
        }
        None => usage(),
    }
}
